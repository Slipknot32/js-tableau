/* exercice numéro 1

Afficher "oui" si le prénom saisis fait plus de 8 caractères, "non" en cas contraire.
Afficher "Banco !", si le prénom saisis fait plus de 10 caractère.
Afficher "Bonjour " + le prénom */

var prenom = "mylène"

if(prenom .length <8){
console.log("non !")
}
if (prenom .length >8)
  {
    console.log ("oui !");
  }
if (prenom .length >10)
  {
    console.log ("Banco !");
  }

console.log("Bonjour" + " " + prenom);

/* exercice numéro 2

Créer une fonction qui prend deux valeurs en entrée et écrit dans la console
si leur type est identique ou pas, pariel pour leurs valeurs. */


function type (opt1 = 2, opt2 = "3"){
  if (typeof opt1 == typeof opt2){
    console.log("le type est le même")
  }
  else{
    console.log("le type est différent")
  }

  if (opt1 === opt2){
    console.log("la valeur est la même")
  }
  else{
    console.log("la veleur est différent")
  }
}
console.log(type())

/*exercice numéro 3 
compter le nombre de croix tab 1 dimention*/

var tab0 = ["X", "O", "O", "X", "Z", 6, null, "X"];
var c = 0;

for (var i=0; i< tab0.length; i++ ){

  if(tab0[i]=="X")
  {c++;
  /*console.log(tab0[i]);*/
  }
}
console.log(c);

/*////////////////////2 dimensions////////////////*/

var tab1 = [
	["X", "O", "X"],
	["O", "O", "X"],
	["X", "O", "O"]
];

var c = 0;

for (var i=0; i<tab1.length; i++ )
{
  for (var j=0; j<tab1.length; j++ )
  {
    if(tab1[i][j] == "X")
    {
      c++;
    }   
  }  
}
console.log(c);


/*////////////////////2dim ++ //////////////////////*/

var tab2 = [
	["X" ,"O", "X"],
	["O" ,"O", "X"],
	["X" ,"O", "O", "Z"]
];

var c = 0;

for (var i=0; i<tab2.length; i++ )
{
  for (var j=0; j<tab2.length+1; j++ )
  {
    if(tab2[i][j] == "X")
    {
      c++;
    }   
  }  
}
console.log(c);

/*///////////////////3 dim /////////////////////////*/

var tab3 = [
	[ ["X","R"], ["T","Z"], ["X","Y"], ["U","F"] ],
	[ ["Q","R"], ["X","Z"], ["X","Y"], ["X","F"] ],
	[ ["V","S"], ["T","X"], ["O","Y"], ["X","F"] ],
	[ ["E","X"], ["Q","Z"], ["X","Y"], ["U","F"] ]
];

var c = 0;

for (var i=0; i<tab3.length; i++ )
{
  for (var j=0; j<tab3[i].length; j++ )
  {
    for (var k=0; k<tab3[i][j].length; k++ )
    {
      if(tab3 [i][j][k] == "X")
      {
        c++;
      }  
    } 
  }  
}
console.log(c);

/*////////////////